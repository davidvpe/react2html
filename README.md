# React 2 HTML

Hi! A few months ago I started working with React (CRA), then moved over to Next.js and at the end I had to work doing html templates for some clients. After being so comfortable using components and wihout much luck trying to generate components using WebComponents and decided to do something about it.

This is React 2 HTML a simple and short utility that gathers all the possible pages and components and convert all of them into pure HTML with inline styles only (for now)

## Main Purpose

Create a tool that lets you create a pure html/css/js project using React Components without the bunch of js and min.css files that Next.js or CRA generates for the webpage

## Structure

```
↳dist		   // Folder where all the generated html files will be
↳public		   // Folder that will containe the html template for all pages
↳src		   // Source Folder, you can add as many of these as you want
    ↳components	   // Folder for all components
    ↳pages	   // [SENSITIVE] Folder for all the pages you'll work on
```

## How to use

In the pages folder you can add as many pages as you want as long as you don't change the name of the folder or the script will fail.
This pages can make use of the components like you do in CRA and Next.js

## TODO

I just started this today (03/19/2021) So there is plenty of stuff to do

-   Support adding styles as CSS Modules and compile these styles to be rendered as `<style></style>` blocks or separated css files
-   Support assets (images) inclusion from the jsx files inside pages or components
-   Support vanilla js generation from typescript (still thinking about the best way to work with this)
-   Still thinking about more...
