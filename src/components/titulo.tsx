import React from 'react'

interface Props {}

const Subtitulo = () => {
    return <h3>Subtitulo!!!!!</h3>
}

const Titulo = (props: Props) => {
    return (
        <div>
            <h1>Titulo!!!</h1>
            <Subtitulo />
        </div>
    )
}

export default Titulo
