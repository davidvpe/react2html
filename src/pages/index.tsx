import React from 'react'
import Titulo from '../components/titulo'

interface Props {}

const Cuadro = (props: Props) => {
    return (
        <div
            style={{
                backgroundColor: 'red',
                width: '300px',
                height: '300px',
            }}
        >
            <Titulo />
        </div>
    )
}

export default Cuadro
