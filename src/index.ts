import fs from 'fs/promises'
import fsSync from 'fs'
import path from 'path'
import ReactDOMServer from 'react-dom/server'

//   _____                 _             _    _ _______ __  __ _
//  |  __ \               | |            | |  | |__   __|  \/  | |
//  | |__) |___  __ _  ___| |_    __ _   | |__| |  | |  | \  / | |
//  |  _  // _ \/ _` |/ __| __|  / _` |  |  __  |  | |  | |\/| | |
//  | | \ \  __/ (_| | (__| |_  | (_| |  | |  | |  | |  | |  | | |____
//  |_|  \_\___|\__,_|\___|\__|  \__,_|  |_|  |_|  |_|  |_|  |_|______|

// NO TOCAR ESTE ARCHIVO / DO NOT TOUCH THIS FILE

const createPageFile = ({
    name,
    htmlGenerated,
}: {
    name: string
    htmlGenerated: string
}) => {
    fs.readFile(path.resolve(__dirname, '../public/index.html'))
        .then((buff) => buff.toString('utf-8'))
        .then((html) =>
            html.replace('<body></body>', `<body>${htmlGenerated}</body>`)
        )
        .then((html) =>
            fs.writeFile(
                path.resolve(__dirname, `../dist/${name}.html`),
                Buffer.from(html, 'utf-8')
            )
        )
}

const workOnFiles = (files: string[]) => {
    return files.map((page: string) => {
        let comp = require(path.resolve(__dirname, `pages/${page}`))
        return {
            name: path.parse(page).name,
            htmlGenerated: ReactDOMServer.renderToStaticMarkup(comp.default()),
        }
    })
}

let distDir = path.resolve(__dirname, '../dist')

new Promise((res, rej) => {
    fs.readdir(distDir)
        .then(() => res(null))
        .catch(() => {
            fs.mkdir(distDir).then(() => res(null))
        })
})
    .then(() => fs.readdir(path.resolve(__dirname, 'pages')))
    .then(workOnFiles)
    .then((pages) => pages.forEach((page) => createPageFile(page)))
    .catch()
